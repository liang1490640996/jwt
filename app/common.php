<?php
// 应用公共文件

use \Firebase\JWT\JWT;


//生成验签
function signToken($uid) {
	# 这里是自定义的一个随机字串，应该写在config文件中的，解密时也会用，相当于加密中常用的 盐  salt
    $key = 'fwefesd7efdsfas99faewfjvsldmfva02rafvdas';
    # payload 自定义值
    $payload = [
        "iss" => $key, //签发者 可以为空
        "aud" => '',   //面象的用户，可以为空
        "iat" => time(),    //签发时间
        "nbf" => time()+3,    //在什么时候jwt开始生效（这里表示生成100秒后才生效）
        "exp" => time()+200, //token 过期时间
        "data" => [
        	// 记录用户的信息如 userid，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
            'uid' => $uid
        ]
    ];
    $jwt = JWT::encode($payload, $key, "HS256");  //根据参数生成了 token
    return $jwt;
}

# 创建token 简单写法
function createToken($user_id, $user_name) {
	# 加密算法
	$algorithm = 'HS256';
	# 构造盐
	$salt = 'fwefesd7efdsfas99faewfjvsldmfva02rafvdas';
	# 构造 header
	$header = [
		'typ' => 'jwt',
		'alg' => 'HS256'
	];
	# 构造 payload
	$payload = [
		'user_id'   => $user_id, # 自定义用户id
		'user_name' => $user_name, # 自定义用户名
		'exp'       => time() + 60 # token过期时间
	];
	return JWT::encode($payload, $salt, $algorithm, $header);

}

//验证token
function checkToken($token) {
    $key = 'fwefesd7efdsfas99faewfjvsldmfva02rafvdas'; 
    $result = ["code" => -1];
    try {
        JWT::$leeway = 60; //当前时间减去60，把时间留点余地
        $decoded = JWT::decode($token, $key, ['HS256']); //HS256方式，这里要和签发的时候对应
        $dataArr = (array)$decoded;
        $result['code'] = 0;
        $result['data'] = $dataArr['data'];
        return $result;
 
    } catch(\Firebase\JWT\SignatureInvalidException $e) { //签名不正确
        $result['msg'] = "签名不正确";
        // $result['e']   = $e->getMessage(); 
        return $result;
    }catch(\Firebase\JWT\BeforeValidException $e) { // 签名在某个时间点之后才能用
        $result['msg'] = "token失效";
        // $result['e']   = $e->getMessage(); 
        return $result;
    }catch(\Firebase\JWT\ExpiredException $e) { // token过期
        $result['msg'] = "token失效";
        // $result['e']   = $e->getMessage(); 	
        return $result;
    }catch(Exception $e) { //其他错误
        $result['msg'] = "未知错误";
        // $result['e']   = $e->getMessage();
        return $result;
    }
}
